import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';

import {NavbarComponent} from './components/navbar/navbar.component';
import {RoleCardComponent} from './components/role-card/role-card.component';
import {LoginComponent} from './components/login/login.component';
import {HomeComponent} from './components/home/home.component';
import {ElectionsComponent} from './components/elections/elections.component';
import {SageHomeComponent} from './components/sage-home/sage-home.component';
import {LightsComponent} from './components/lights/lights.component';
import {EventPageComponent} from './components/event-page/event-page.component';
import {SubscribeComponent} from './components/subscribe/subscribe.component';
import {CreateEventComponent} from './components/create-event/create-event.component';
import {ReportProblemComponent} from './components/report-problem/report-problem.component';
import {ProblemsPageComponent} from './components/problems-page/problems-page.component';
import {ProblemComponent} from './components/problem/problem.component';
import {RegistrationComponent} from './components/registration/registration.component';

import {EqualValidatorDirective} from './components/shared/equal.validator.directive';
import {AuthService} from './auth/auth.service';
import {AdminGuardGuard} from './auth/admin-guard.guard';
import {ContabilGuardGuard} from './auth/contabil-guard.guard';
import {CasierGuardGuard} from './auth/casier-guard.guard';
import {LocatarGuardGuard} from './auth/locatar-guard.guard';
import {PresidentGuardGuard} from './auth/president-guard.guard';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Material} from './material/material';
import { InregistrareConsumComponent } from './components/inregistrare-consum/inregistrare-consum.component';
import { ModUtilizareComponent } from './components/mod-utilizare/mod-utilizare.component';
import { PlataOnlineComponent } from './components/plata-online/plata-online.component';
import {UserPageComponent} from './components/user-page/user-page.component';
import {AdminPageComponent} from './components/admin-page/admin-page.component';
import {AbnormalitiesComponent} from './components/abnormalities/abnormalities.component';
import {NoticeBoardComponent} from './components/notice-board/notice-board.component';

const appRoutes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegistrationComponent},
  {path: '', component: HomeComponent, pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'elections', component: ElectionsComponent, canActivate: [LocatarGuardGuard]},
  {path: 'sage-home', component: SageHomeComponent, canActivate: [LocatarGuardGuard]},
  {path: 'lights', component: LightsComponent, canActivate: [AdminGuardGuard]},
  {path: 'create-event', component: CreateEventComponent, canActivate: [LocatarGuardGuard]},
  {path: 'subscribe', component: SubscribeComponent, canActivate: [LocatarGuardGuard]},
  {path: 'event', component: EventPageComponent, canActivate: [LocatarGuardGuard]},
  {path: 'report-problem', component: ReportProblemComponent, canActivate: [LocatarGuardGuard]},
  {path: 'problems', component: ProblemsPageComponent, canActivate: [AdminGuardGuard]},
  {path: 'consum', component: InregistrareConsumComponent, canActivate: [LocatarGuardGuard]},
  {path: 'utilizare', component: ModUtilizareComponent, canActivate: [LocatarGuardGuard]},
  {path: 'plata', component: PlataOnlineComponent, canActivate: [LocatarGuardGuard]},
  {path: 'abn', component: AbnormalitiesComponent, canActivate: [LocatarGuardGuard]},
  {path: 'notice-board', component: NoticeBoardComponent, canActivate: [LocatarGuardGuard]},
  {path: 'user', component: UserPageComponent, canActivate: [LocatarGuardGuard]},
  {path: 'admin', component: AdminPageComponent, canActivate: [AdminGuardGuard]}
];

@NgModule({
  declarations: [
    AppComponent,
    EqualValidatorDirective,
    NavbarComponent,
    RoleCardComponent,
    LoginComponent,
    HomeComponent,
    ElectionsComponent,
    SageHomeComponent,
    LightsComponent,
    EventPageComponent,
    SubscribeComponent,
    CreateEventComponent,
    ReportProblemComponent,
    ProblemsPageComponent,
    ProblemComponent,
    RegistrationComponent,
    InregistrareConsumComponent,
    ModUtilizareComponent,
    PlataOnlineComponent,
    UserPageComponent,
    AdminPageComponent,
    NoticeBoardComponent,
    AbnormalitiesComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    Material,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [AuthService, AdminGuardGuard, ContabilGuardGuard, CasierGuardGuard, LocatarGuardGuard, PresidentGuardGuard],
  bootstrap: [AppComponent],
})
export class AppModule {
}
