export class User {
  id: any;
  username: string;
  email: string;
  password: string;
  userRole: string;
  profileImage: string;
  phoneNumber: string;
  firstName: string;
  lastName: string;

  constructor(id: number, email: string, userRole: string, profileImage: string, phoneNumber: string,
              firstName: string,  lastName: string, username: string, password: string) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.email = email;
    this.userRole = userRole;
    this.profileImage = profileImage;
    this.phoneNumber = phoneNumber;
    this.firstName = firstName;
    this.lastName = lastName;
  }
}
