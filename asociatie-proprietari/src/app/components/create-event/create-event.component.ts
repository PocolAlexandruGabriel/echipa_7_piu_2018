import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {
  eventName: string;

  constructor() { }

  ngOnInit() {
    document.getElementById('event-name-error').style.display = 'none';
  }

  createEvent() {
    if (confirm('Sunteți sigur că doriți să creați evenimentul?')) {
      if (this.eventName != null && this.eventName !== '') {
        alert('Evenimentul a fost creat cu succes!');
        document.getElementById('event-name-error').style.display = 'none';
        window.location.href = '/user';
      } else {
        alert('A apărut o eroare. Vă rugăm să verificați datele introduse!');
        document.getElementById('event-name-error').style.display = 'block';
      }
    }
  }
}
