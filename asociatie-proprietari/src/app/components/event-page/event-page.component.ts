import {Component, OnInit} from '@angular/core';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-event-page',
  templateUrl: './event-page.component.html',
  styleUrls: ['./event-page.component.css']
})
export class EventPageComponent implements OnInit {
  chatMessage: string;
  donatedAmount: string;

  constructor() {
  }

  ngOnInit() {
  }

  sendMessage() {
    if (this.chatMessage != null) {
      document.getElementById('chat-box').innerHTML += '&#13;&#10;Me: ' + this.chatMessage;
      this.chatMessage = null;
    }
  }

  donate() {
    if (this.donatedAmount != null) {
      if (confirm('Sunteți sigur că doriți să donați această sumă pentru eveniment?')) {
        document.getElementById('history-box').innerHTML += '&#13;&#10;' + formatDate(new Date(), 'dd/MM/yyyy HH:mm',
          'en') + ': Ați donat ' + this.donatedAmount + ' LEI pentru eveniment';
        this.donatedAmount = null;
      }
    }
  }

  viewDescription() {
    alert('Deszăpezire în jurul blocului în data de 30 decembrie 2018 începând cu ora 12:00');
  }
}
