import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-report-problem',
  templateUrl: './report-problem.component.html',
  styleUrls: ['./report-problem.component.css']
})
export class ReportProblemComponent implements OnInit {
  problemName: string;

  constructor() {
  }

  ngOnInit() {
    document.getElementById('problem-name-error').style.display = 'none';
  }

  reportProblem() {
    if (confirm('Sunteți sigur că doriți să raportati problema?')) {
      if (this.problemName != null && this.problemName !== '') {
        alert('Problema a fost raportată cu succes!');
        document.getElementById('problem-name-error').style.display = 'none';
        window.location.href = '/user';
      } else {
        alert('A apărut o eroare. Vă rugăm să verificați datele introduse!');
        document.getElementById('problem-name-error').style.display = 'block';
      }
    }
  }
}
