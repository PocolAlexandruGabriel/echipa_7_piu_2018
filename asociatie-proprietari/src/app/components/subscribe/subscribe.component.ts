import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.css']
})
export class SubscribeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  subscribe() {
    if (confirm('Sunteți sigur că doriți să vă abonați?')) {
      alert('V-ați abonat cu success la știrile blocului!');
      window.location.href = '/user';
    }
  }
}
