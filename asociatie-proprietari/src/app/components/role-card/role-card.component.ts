import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-role-card',
  templateUrl: './role-card.component.html',
  styleUrls: ['./role-card.component.css']
})
export class RoleCardComponent implements OnInit {

  @Input() role: string;
  @Input() text: string;

  constructor() {
  }

  ngOnInit() {
  }

}
