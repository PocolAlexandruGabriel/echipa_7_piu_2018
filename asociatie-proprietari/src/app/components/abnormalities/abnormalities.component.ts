import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-abnormalities',
  templateUrl: './abnormalities.component.html',
  styleUrls: ['./abnormalities.component.css']
})
export class AbnormalitiesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  stopWater() {
      if (confirm('Opriti apa?')) {
        alert('Apa s-a oprit cu succes!');
      }
  }

  stopGas() {
    if (confirm('Opriti gas?')) {
      alert('Gazul s-a oprit cu succes!');
    }
  }

  callWater() {
    if (confirm('Contactati compania de apa?')) {
      alert('Campania de apa a fost sunata. Timpul mediu de asteptare pana la sosirea unei echipe este de 30 de minute');
    }
  }

  callGas() {
    if (confirm('Contactati compania de gas?')) {
      alert('Campania de gas a fost sunata. Timpul mediu de asteptare pana la sosirea unei echipe este de 20 de minute');
    }
  }
}
