import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbnormalitiesComponent } from './abnormalities.component';

describe('AbnormalitiesComponent', () => {
  let component: AbnormalitiesComponent;
  let fixture: ComponentFixture<AbnormalitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbnormalitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbnormalitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
