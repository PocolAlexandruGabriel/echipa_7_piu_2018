import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-plata-online',
  templateUrl: './plata-online.component.html',
  styleUrls: ['./plata-online.component.css']
})
export class PlataOnlineComponent implements OnInit {

  cont = 245;
  depunere: number;
  numarCard: number;
  error = false;
  eventName: string;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  depune(): void {
    if (this.numarCard) {
      this.cont += this.depunere;
      this.error = false;
      this.createEvent();
    } else {
      this.error = true;
    }
  }

  plateste(): void {
    if (this.numarCard) {
      this.createEvent();
      this.router.navigate(['/user']);
    } else {
      this.error = true;
    }
  }

  createEvent() {
    if (confirm('Sunteți sigur că doriți să efectuati aceasta operatiune?')) {
        alert('Operatiunea a fost realizata cu succes!');
    }
  }
}
