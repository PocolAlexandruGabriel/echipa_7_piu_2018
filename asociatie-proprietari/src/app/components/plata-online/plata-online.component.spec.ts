import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlataOnlineComponent } from './plata-online.component';

describe('PlataOnlineComponent', () => {
  let component: PlataOnlineComponent;
  let fixture: ComponentFixture<PlataOnlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlataOnlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlataOnlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
