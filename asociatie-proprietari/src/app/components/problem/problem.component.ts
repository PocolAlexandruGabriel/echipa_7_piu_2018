import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-problem',
  templateUrl: './problem.component.html',
  styleUrls: ['./problem.component.css']
})
export class ProblemComponent implements OnInit {
  @Input() problemName: string;
  @Input() priority: string;
  @Input() author: string;
  @Input() reportDate: string;
  @Input() description = 'Descriere indisponibilă';
  @Input() index: any;

  constructor() {
  }

  ngOnInit() {
  }

  viewDescription() {
    alert(this.description);
  }

  showImage() {
    alert('Imagine indisponibilă');
  }

  solveProblem() {
    if (confirm('Sunteți sigur că doriți să marcați acestă problemă ca fiind rezolvată?')) {
      document.getElementsByName('status').item(this.index).style.backgroundColor = 'green';
      document.getElementsByName('status').item(this.index).innerHTML = 'Rezolvată';
    }
  }

  remove() {
    if (confirm('Sunteți sigur că doriți să ștergeți acestă problemă')) {
      document.getElementsByName('problem').item(this.index).style.display = 'none';
    }
  }
}
