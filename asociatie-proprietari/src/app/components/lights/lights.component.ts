import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lights',
  templateUrl: './lights.component.html',
  styleUrls: ['./lights.component.css']
})
export class LightsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  createEvent() {
    if (confirm('Salvati setarile?')) {
        alert('Setarile s-au salvat cu succes!');
        window.location.href = '/admin';
    }
  }
}
