import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-elections',
  templateUrl: './elections.component.html',
  styleUrls: ['./elections.component.css']
})
export class ElectionsComponent implements OnInit {

  eventName: string;
  even1: string;
  even2: string;
  even3: string;
  even4: string;

  constructor() { }

  ngOnInit() {
  }

  vote1() {
    if (confirm('Sunteți sigur că doriți să inregistrati votul?')) {
        if (this.eventName != null && this.eventName !== '' && this.even1 != null && this.even1 !== '') {
          alert('Votul a fost inregistrat cu succes!');
          document.getElementById('event-name-error').style.display = 'none';
          // window.location.href = '/elections';
        } else {
          alert('A apărut o eroare. Vă rugăm să verificați ca ati bifat intelegerea regulamentului si ca ati selectat un candidat!');
          document.getElementById('event-name-error').style.display = 'block';
        }
    }
  }

  vote2() {
    if (confirm('Sunteți sigur că doriți să inregistrati votul?')) {
      if (this.eventName != null && this.eventName !== '' && this.even2 != null && this.even2 !== '') {
        alert('Votul a fost inregistrat cu succes!');
        document.getElementById('event-name-error').style.display = 'none';
        // window.location.href = '/elections';
      } else {
        alert('A apărut o eroare. Vă rugăm să verificați datele introduse!');
        document.getElementById('event-name-error').style.display = 'block';
      }
    }
  }

  vote3() {
    if (confirm('Sunteți sigur că doriți să inregistrati votul?')) {
      if (this.eventName != null && this.eventName !== '' && this.even3 != null && this.even3 !== '') {
        alert('Votul a fost inregistrat cu succes!');
        document.getElementById('event-name-error').style.display = 'none';
        // window.location.href = '/elections';
      } else {
        alert('A apărut o eroare. Vă rugăm să verificați datele introduse!');
        document.getElementById('event-name-error').style.display = 'block';
      }
    }
  }

  vote4() {
    if (confirm('Sunteți sigur că doriți să inregistrati votul?')) {
      if (this.eventName != null && this.eventName !== '' && this.even4 != null && this.even4 !== '') {
        alert('Votul a fost inregistrat cu succes!');
        document.getElementById('event-name-error').style.display = 'none';
        // window.location.href = '/elections';
      } else {
        alert('A apărut o eroare. Vă rugăm să verificați datele introduse!');
        document.getElementById('event-name-error').style.display = 'block';
      }
    }
  }
}
