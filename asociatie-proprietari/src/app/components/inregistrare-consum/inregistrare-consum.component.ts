import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-inregistrare-consum',
  templateUrl: './inregistrare-consum.component.html',
  styleUrls: ['./inregistrare-consum.component.css']
})
export class InregistrareConsumComponent implements OnInit {

  consum: number;
  procent = 30;
  show = false;

  constructor() {
  }

  ngOnInit() {
  }

  adaugare(): void {
    if (this.consum) {
      this.show = true;
      this.procent = 25;
      this.createEvent();
    }
  }

  createEvent() {
    if (confirm('Sunteți sigur că inregistrati acest consum ?')) {
      if (this.consum != null && this.consum !== 0) {
        alert('Consumul a fost inregistrat cu succes!');
        document.getElementById('event-name-error').style.display = 'none';
        // window.location.href = '/user';
      } else {
        alert('A apărut o eroare. Vă rugăm să verificați datele introduse!');
        document.getElementById('event-name-error').style.display = 'block';
      }
    }
  }
}
