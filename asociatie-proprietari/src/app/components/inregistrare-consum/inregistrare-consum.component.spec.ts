import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InregistrareConsumComponent } from './inregistrare-consum.component';

describe('InregistrareConsumComponent', () => {
  let component: InregistrareConsumComponent;
  let fixture: ComponentFixture<InregistrareConsumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InregistrareConsumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InregistrareConsumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
