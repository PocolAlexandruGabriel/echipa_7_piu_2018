import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-mod-utilizare',
  templateUrl: './mod-utilizare.component.html',
  styleUrls: ['./mod-utilizare.component.css']
})
export class ModUtilizareComponent implements OnInit {

  eventName: string;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToHome(): void {
    this.createEvent();
    this.router.navigate(['/home']);
  }

  createEvent() {
    if (confirm('Sunteți sigur că doriți să salvati aceasta optiune?')) {
        alert('Optiunea a fost creata cu succes!');
        // document.getElementById('event-name-error').style.display = 'none';
        window.location.href = '/user';
    }
  }
}
