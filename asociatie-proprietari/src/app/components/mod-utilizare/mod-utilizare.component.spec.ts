import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModUtilizareComponent } from './mod-utilizare.component';

describe('ModUtilizareComponent', () => {
  let component: ModUtilizareComponent;
  let fixture: ComponentFixture<ModUtilizareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModUtilizareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModUtilizareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
