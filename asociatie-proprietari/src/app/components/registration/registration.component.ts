import { OnInit, Component } from '@angular/core';

import {User} from '../../model/user.model';
import {Title} from '@angular/platform-browser';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  registerForm: FormGroup;

  constructor(private title: Title, private router: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.title.setTitle('Registration');
    this.createForm();
  }

  createForm() {
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      password: ['', Validators.required]
    });
  }

  register(): void {
    const user: User = new User(null, this.registerForm.value.email, null, null, null,
      null, null, this.registerForm.value.username, this.registerForm.value.password);
    this.router.navigateByUrl('utilizare');
  }
}


