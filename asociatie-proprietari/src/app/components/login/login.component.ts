import {Component, OnInit} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../auth/auth.service';
import {User} from '../../model/user.model';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User;
  errorMessage = '';
  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router,
              private title: Title, private auth: AuthService) {
  }

  ngOnInit() {
    this.title.setTitle('Login');
    this.createForm();
  }

  createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', Validators.required],
    });
  }

  logIn(): void {
    if (this.loginForm.value.username === 'admin' && this.loginForm.value.password === 'admin') {
      this.auth.sendTokenAdmin(this.loginForm.value.username);
      this.router.navigateByUrl('admin');
    } else if (this.loginForm.value.username === 'admin1' && this.loginForm.value.password === 'admin1') {
      this.auth.sendTokenAdmin(this.loginForm.value.username);
      this.router.navigateByUrl('admin');
    } else if (this.loginForm.value.username === 'pres' && this.loginForm.value.password === 'pres') {
      this.auth.sendTokenPresident(this.loginForm.value.username);
      this.router.navigateByUrl('home');
    } else if (this.loginForm.value.username === 'casier' && this.loginForm.value.password === 'casier') {
      this.auth.sendTokenCasier(this.loginForm.value.username);
      this.router.navigateByUrl('home');
    } else if (this.loginForm.value.username === 'contabil' && this.loginForm.value.password === 'contabil') {
      this.auth.sendTokenContabil(this.loginForm.value.username);
      this.router.navigateByUrl('home');
    } else if (this.loginForm.value.username === 'locatar' && this.loginForm.value.password === 'locatar') {
      this.auth.sendTokenLocatar(this.loginForm.value.username);
      this.router.navigateByUrl('user');
    } else if (this.loginForm.value.username === 'locatar1' && this.loginForm.value.password === 'lacatar1') {
      this.auth.sendTokenLocatar(this.loginForm.value.username);
      this.router.navigateByUrl('user');
    } else {
      this.errorMessage = 'Incorrect username or password!';
    }
  }
}
