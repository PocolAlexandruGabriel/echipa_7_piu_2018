import { TestBed, async, inject } from '@angular/core/testing';

import { PresidentGuardGuard } from './president-guard.guard';

describe('PresidentGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PresidentGuardGuard]
    });
  });

  it('should ...', inject([PresidentGuardGuard], (guard: PresidentGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
