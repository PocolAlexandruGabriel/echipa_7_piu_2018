import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class PresidentGuardGuard implements CanActivate {

  constructor(private auth: AuthService,
              private myRoute: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.auth.isLoggedInAsPresident()) {
      return true;
    } else {
      if (this.auth.isLoggedInAsAdmin()) {
        this.auth.logoutAdmin();
      } else
      if (this.auth.isLoggedInAsLocatar()) {
        this.auth.logoutLocatar();
      } else
      if (this.auth.isLoggedInAsPresident()) {
        this.auth.logoutPresident();
      } else
      if (this.auth.isLoggedInAsCasier()) {
        this.auth.logoutCasier();
      } else
      if (this.auth.isLoggedInAsContabil()) {
        this.auth.logoutContabil();
      }
      this.myRoute.navigate(['login']);
      return false;
    }
  }
}
