import { TestBed, async, inject } from '@angular/core/testing';

import { CasierGuardGuard } from './casier-guard.guard';

describe('CasierGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CasierGuardGuard]
    });
  });

  it('should ...', inject([CasierGuardGuard], (guard: CasierGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
