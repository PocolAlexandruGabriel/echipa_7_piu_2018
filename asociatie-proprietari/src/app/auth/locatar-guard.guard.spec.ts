import { TestBed, async, inject } from '@angular/core/testing';

import { LocatarGuardGuard } from './locatar-guard.guard';

describe('LocatarGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocatarGuardGuard]
    });
  });

  it('should ...', inject([LocatarGuardGuard], (guard: LocatarGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
