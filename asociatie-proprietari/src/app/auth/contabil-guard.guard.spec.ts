import { TestBed, async, inject } from '@angular/core/testing';

import { ContabilGuardGuard } from './contabil-guard.guard';

describe('ContabilGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContabilGuardGuard]
    });
  });

  it('should ...', inject([ContabilGuardGuard], (guard: ContabilGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
