import { Injectable } from '@angular/core';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private myRoute: Router) { }

  // admin services
  sendTokenAdmin(token: string) {
    localStorage.setItem('admin', token);
  }

  getTokenAdmin() {
    return localStorage.getItem('admin');
  }

  isLoggedInAsAdmin() {
    return this.getTokenAdmin() !== null;
  }

  logoutAdmin() {
    localStorage.removeItem('admin');
    this.myRoute.navigate(['login']);
  }

  // president services
  sendTokenPresident(token: string) {
    localStorage.setItem('president', token);
  }

  getTokenPresident() {
    return localStorage.getItem('president');
  }

  isLoggedInAsPresident() {
    return this.getTokenPresident() !== null;
  }

  logoutPresident() {
    localStorage.removeItem('president');
    this.myRoute.navigate(['login']);
  }

  // contabil services
  sendTokenContabil(token: string) {
    localStorage.setItem('contabil', token);
  }

  getTokenContabil() {
    return localStorage.getItem('contabil');
  }

  isLoggedInAsContabil() {
    return this.getTokenContabil() !== null;
  }

  logoutContabil() {
    localStorage.removeItem('contabil');
    this.myRoute.navigate(['login']);
  }

  // casier services
  sendTokenCasier(token: string) {
    localStorage.setItem('casier', token);
  }

  getTokenCasier() {
    return localStorage.getItem('casier');
  }

  isLoggedInAsCasier() {
    return this.getTokenCasier() !== null;
  }

  logoutCasier() {
    localStorage.removeItem('casier');
    this.myRoute.navigate(['login']);
  }

  // locatar services
  sendTokenLocatar(token: string) {
    localStorage.setItem('locatar', token);
  }

  getTokenLocatar() {
    return localStorage.getItem('locatar');
  }

  isLoggedInAsLocatar() {
    return this.getTokenLocatar() !== null;
  }

  logoutLocatar() {
    localStorage.removeItem('locatar');
    this.myRoute.navigate(['login']);
  }
}
